﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P3LINQ
{
    class Program
    {
        /// <summary>
        /// Funkcja przygotowująca trójwymiarową tablicę poszarpaną z arbitralnymi danymi do dalszego przetwarzania.
        /// </summary>
        /// <returns>Trójwymiarowa tablica poszarpana, wypełniona obiektami typu Książka o przypadkowych wartościach</returns>
        static Ksiazka[][][] PrzygotujTablicePoszarpana()
        {
            // Inicjalizacja pierwszego wymiaru tablicy
            Ksiazka[][][] tablica = new Ksiazka[3][][];
            for (int i = 0; i < tablica.Length; i++)
            {
                // Inicjalizacja drugiego wymiaru tablicy
                tablica[i] = new Ksiazka[6][];
                for (int j = 0; j < tablica[i].Length; j++)
                {
                    // Inicjalizacja trzeciego wymiaru tablicy
                    tablica[i][j] = new Ksiazka[10];
                    for (int k = 0; k < tablica[i][j].Length; k++)
                    {
                        // Wpisanie do tablicy książki o przypadkowych danych
                        tablica[i][j][k] = new Ksiazka("Sienkiewicz","Potop", i, j, k);
                    }
                }
            }
            // Podmiana jednej z książek, aby można było jej potem szukać.
            tablica[2][3][3] = new Ksiazka("Symfonia C++", "???", 2, 3, 3);
            return tablica;
        }

        /// <summary>
        /// Funkcja przygotowująca trójwymiarową tablicę prostokątną z arbitralnymi danymi do dalszego przetwarzania.
        /// </summary>
        /// <returns>Trójwymiarowa tablica prostokątna, wypełniona obiektami typu Książka o przypadkowych wartościach</returns>
        static Ksiazka[,,] PrzygotujTabliceProstokatna()
        {
            // Inicjalizacja tablicy
            Ksiazka[,,] tablica = new Ksiazka[3,6,10];
            for (int i = 0; i < tablica.GetLength(0); i++)
            {
                for (int j = 0; j < tablica.GetLength(1); j++)
                {
                    for (int k = 0; k < tablica.GetLength(2); k++)
                    {
                        // Wpisanie do tablicy książki o przypadkowych danych
                        tablica[i,j,k] = new Ksiazka("Sienkiewicz", "Potop", i, j, k);
                    }
                }
            }
            // Podmiana jednej z książek, aby można było jej potem szukać.
            tablica[2,3,3] = new Ksiazka("Symfonia C++", "???", 2, 3, 3);
            return tablica;
        }

        /// <summary>
        /// Funkcja wyszukująca książkę, której autor lub tytuł są takie jak podany w parametrze tekst. Wyszukiwanie klasyczne (iteracyjne).
        /// Przeładowanie przyjmujące tablicę poszarpaną
        /// </summary>
        /// <param name="magazyn">Trójwymiarowa tablica poszarpana zawierająca książki</param>
        /// <param name="szukane">Szukana fraza</param>
        /// <returns>Poszukiwana książka lub null jeśli żadna nie została znaleziona</returns>
        static Ksiazka Wyszukaj(Ksiazka[][][] magazyn, string szukane)
        {
            for (int i = 0; i < magazyn.Length; i++)
            {
                for (int j = 0; j < magazyn[i].Length; j++)
                {
                    for (int k = 0; k < magazyn[i][j].Length; k++)
                    {
                        if (magazyn[i][j][k].Autor == szukane || magazyn[i][j][k].Tytul == szukane)
                        {
                            return magazyn[i][j][k];
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Funkcja wyszukująca książkę, której autor lub tytuł są takie jak podany w parametrze tekst. Wyszukiwanie klasyczne (iteracyjne).
        /// Przeładowanie przyjmujące tablicę prostokątną
        /// </summary>
        /// <param name="magazyn">Trójwymiarowa tablica prostokątna zawierająca książki</param>
        /// <param name="szukane">Szukana fraza</param>
        /// <returns>Poszukiwana książka lub null jeśli żadna nie została znaleziona</returns>
        static Ksiazka Wyszukaj(Ksiazka[,,] magazyn, string szukane)
        {
            for (int i = 0; i < magazyn.GetLength(0); i++)
            {
                for (int j = 0; j < magazyn.GetLength(1); j++)
                {
                    for (int k = 0; k < magazyn.GetLength(2); k++)
                    {
                        if (magazyn[i,j,k].Autor == szukane || magazyn[i,j,k].Tytul == szukane)
                        {
                            return magazyn[i,j,k];
                        }
                    }
                }
            }
            return null;
        }

        static void Main(string[] args)
        {
            // Inicjalizacja tablicy (kolekcji)
            Ksiazka[][][] biblioteka = PrzygotujTablicePoszarpana();
            Ksiazka[,,] biblioteka2 = PrzygotujTabliceProstokatna();

            // Wyszukiwanie iteracyjne
            Ksiazka znaleziona = Wyszukaj(biblioteka, "???");
            Console.WriteLine($"Ksiazka znajduje sie na regale {znaleziona.Regal+1}, polce {znaleziona.Polka +1}, miejscu {znaleziona.Miejsce +1}.");

            Ksiazka znaleziona2 = Wyszukaj(biblioteka2, "???");
            Console.WriteLine($"Ksiazka znajduje sie na regale {znaleziona2.Regal + 1}, polce {znaleziona2.Polka + 1}, miejscu {znaleziona2.Miejsce + 1}.");

            // Zamiana tablicy na listę
            List<Ksiazka> lista = biblioteka.SelectMany(x => x.SelectMany(y => y)).ToList(); // dla tablicy poszarpanej [][][]
            List<Ksiazka> lista2 = biblioteka2.Cast<Ksiazka>().ToList(); // dla tablicy prostokątnej [ , , ]

            // Wyszukiwanie książki przy pomocy LINQ
            // Identyczne dla obu list
            znaleziona = lista
                .Where(x => x.Autor == "???" || x.Tytul == "???")
                .FirstOrDefault();

            Console.WriteLine($"Ksiazka znajduje sie na regale {znaleziona.Regal + 1}," +
                $" polce {znaleziona.Polka + 1}, miejscu {znaleziona.Miejsce + 1}.");
        }
    }
}
