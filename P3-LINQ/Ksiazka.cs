﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P3LINQ
{
    /// <summary>
    /// Klasa modelująca książkę w bibliotece
    /// </summary>
    public class Ksiazka
    {
        /// <summary>
        /// Tytuł książki
        /// </summary>
        public string Tytul;
        /// <summary>
        /// Autor książki
        /// </summary>
        public string Autor;
        /// <summary>
        /// Numer regału, na którym stoi książka
        /// </summary>
        public int Regal;
        /// <summary>
        /// Numer półki regału, na którym stoi książka
        /// </summary>
        public int Polka;
        /// <summary>
        /// Numer kolejny książki na półce
        /// </summary>
        public int Miejsce;

        /// <summary>
        /// Pełny konstruktor
        /// </summary>
        /// <param name="tytul">Tytuł książki</param>
        /// <param name="autor">Autor książki</param>
        /// <param name="regal">Numer regału, na którym stoi książka</param>
        /// <param name="polka">Numer półki regału, na którym stoi książka</param>
        /// <param name="miejsce">Numer kolejny książki na półce</param>
        public Ksiazka(
            string tytul, 
            string autor, 
            int regal, 
            int polka, 
            int miejsce)
        {
            Tytul = tytul;
            Autor = autor;
            Regal = regal;
            Polka = polka;
            Miejsce = miejsce;
        }
    }
}
